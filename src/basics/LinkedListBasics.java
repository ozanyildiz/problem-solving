package basics;

import leetcode.ListNode;

public class LinkedListBasics {

	public static ListNode append(ListNode head, int data) {
		ListNode newNode = new ListNode(data);
		if (head == null) {
			return newNode;
		}
		ListNode curr = head;
		while (curr.next != null) {
			curr = curr.next;
		}
		curr.next = newNode;
		return head;
	}

	public static ListNode push(ListNode head, int data) {
		ListNode newNode = new ListNode(data);
		if (head == null) {
			return newNode;
		}
		newNode.next = head;
		head = newNode;
		return head;
	}
	
	public static ListNode insertAfter(ListNode head, int ref, int data) {
		ListNode newNode = new ListNode(data);
		ListNode curr = head;
		while (curr != null) {
			if (curr.val == ref) {
				newNode.next = curr.next;
				curr.next = newNode;
				break;
			}
			curr = curr.next;
		}
		return head;
	}
	
	public static ListNode insertBefore(ListNode head, int ref, int data) {
		ListNode newNode = new ListNode(data);
		ListNode curr = head;
		while (curr.next != null) {
			if (curr.next.val == ref) {
				newNode.next = curr.next;
				curr.next = newNode;
				break;
			}
			curr = curr.next;
		}
		return head;
	}

	public static ListNode insertAfter2(ListNode head, int ref, int data) {
		ListNode curr = head;
		while (curr != null && curr.val != ref) {
			curr = curr.next;
		}
		if (curr == null) {
			return null;
		} else {
			ListNode newNode = new ListNode(data);
			newNode.next = curr.next;
			curr.next = newNode;
			return head;
		}
	}
	
	public static ListNode insertBefore2(ListNode head, int ref, int data) {
		ListNode curr = head;
		ListNode prev = null;
		
		if (head.val == ref) {
			ListNode newNode = new ListNode(data);
			newNode.next = head.next;
			head = newNode;
			return head;
		}
		
		while (curr != null && curr.val != ref) {
			prev = curr;
			curr = curr.next;
		}
		
		if (curr == null) {
			return null;
		} else {
			ListNode newNode = new ListNode(data);
			newNode.next = prev.next;
			prev.next = newNode;
			return head;
		}
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1, new ListNode(2 , new ListNode(3)));
		ListNode.display(head);
		ListNode.display(append(head, 4));
		ListNode.display(push(head, 0));
		ListNode.display(insertAfter(head, 2, 9));
		ListNode.display(insertBefore(head, 3, 8));
		ListNode.display(insertAfter2(head, 1, 5));
		ListNode.display(insertBefore2(head, 1, 0));
		ListNode.display(insertBefore2(head, 3, 10));
	}
}
