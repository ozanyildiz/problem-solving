package leetcode;

public class ReverseLinkedList {

	public static ListNode reverseList(ListNode head) {
		if (head == null) return null;
		int size = size(head);
		ListNode curr = head;
		ListNode iter = head;
		for (int i = 0; i < size / 2; i++) {
			int t = (size - 1) - (i * 2);
			ListNode ref = iter;
			curr = iter;
			for (int j = 0; j < t; j++) {
				curr = curr.next;
			}
			int tmp = ref.val;
			ref.val = curr.val;
			curr.val = tmp;
			iter = iter.next;
		}
		return head;
	}
	
	public static int size(ListNode head) {
		ListNode curr = head;
		int size = 0;
		while (curr != null) {
			size++;
			curr = curr.next;
		}
		return size;
	}

	public static void main(String[] args) {
		ListNode a = new ListNode(8, new ListNode(9, new ListNode(4)));
		ListNode.display(a);
		ListNode.display(reverseList(a));
		ListNode b = new ListNode(8, new ListNode(9, new ListNode(4, new ListNode(10))));
		ListNode.display(b);
		ListNode.display(reverseList(b));
		ListNode c = new ListNode(8);
		ListNode.display(c);
		ListNode.display(reverseList(c));
		ListNode.display(reverseList(null));
	}

}
