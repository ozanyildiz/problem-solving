package leetcode;

public class ZigZagConversion
{

    public static void main(String[] args)
    {
	String s = "PAYPALISHIRING";
	System.out.println(convert(s, 3));
	System.out.println(convert(s, 4));
	System.out.println(convert(s, 5));
    }
    
    public static String everyNthChar(String s, int start, int n) {
	String result = "";
	for (int i = start; i < s.length(); i += n) {
	    result += s.charAt(i);
	}
	return result;
    }
    
    public static String convert(String s, int numRows) {
	String converted = "";
	for (int i = 0; i < numRows; i++) {
	    if (i == 0 || i == numRows - 1) {
                converted += everyNthChar(s, i, numRows + 2);
	    } else {
                converted += everyNthChar(s, i, numRows - 1);
	    }
	}
	return converted;
    }

}
