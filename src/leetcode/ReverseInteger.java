package leetcode;

public class ReverseInteger {

	public static int reverse(int x) {
		String s = String.valueOf(x);
		boolean negative = false;
		if (s.charAt(0) == '-') {
			negative = true;
			s = s.substring(1);
		}
		try {
			int res = Integer.valueOf(new StringBuilder(s).reverse().toString());
			if (negative) return res * -1; else return res;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public static void main(String[] args) {
		System.out.println("123 " + reverse(123));
		System.out.println("-123 " + reverse(-123));
		System.out.println("1230 " + reverse(1230));
		System.out.println("-1230 " + reverse(-1230));
		System.out.println("1534236469 " + reverse(1534236469));
	}

}
