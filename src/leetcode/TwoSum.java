package leetcode;

public class TwoSum {
	public static int[] twoSum(int[] nums, int target) {
		int[] indices = new int[2];
		for (int i = 0; i < nums.length - 1; i++) {
			int d = target - nums[i];
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[j] == d) {
					indices[0] = i;
					indices[1] = j;
					break;
				}
			}
		}
		return indices;
	}

	public static void main(String[] args) {
		int[] n = new int[] { 2, 7, 11, 13 };
		int[] result = twoSum(n, 9);
		System.out.println(result[0] + " " + result[1]);
		int[] n2 = new int[] { -1, -2, -3, -4, -5 };
		int[] result2 = twoSum(n2, -8);
		System.out.println(result2[0] + " " + result2[1]);
	}

}
