package leetcode;

public class AddTwoNumbers {

	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		boolean addOne = false;
		ListNode head = null;
		ListNode current = null;
		while (l1 != null) {
			int sum = l1.val + l2.val;
			if (addOne) {
				sum += 1;
			}
			addOne = sum >= 10;
			sum = sum % 10;
			ListNode newNode = new ListNode(sum);
			if (head == null) {
				head = newNode;
				current = newNode;
			} else {
				current.next = newNode;
				current = newNode;
			}
			l1 = l1.next;
			l2 = l2.next;
		}
		if (addOne) {
			ListNode one = new ListNode(1);
			current.next = one;
		}
		return head;
	}

	public static void main(String[] args) {
		ListNode a = new ListNode(8, new ListNode(9, new ListNode(9)));
		ListNode b = new ListNode(2, new ListNode(2, new ListNode(1)));
		// ListNode ab = addTwoNumbers(a, b);
		// display(ab);
		ListNode c = new ListNode(2, new ListNode(4, new ListNode(3)));
		ListNode d = new ListNode(5, new ListNode(6, new ListNode(4)));
		ListNode cd = addTwoNumbers(c, d);
		ListNode.display(cd);
	}
}
