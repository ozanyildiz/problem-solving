package leetcode;

public class RemoveLinkedListElements {

	public static ListNode removeElements(ListNode head, int val) {
		if (head == null) {
			return null;
		}
		
		if (head.val == val) {
			head = head.next;
			if (head == null) return head;
		}

		ListNode curr = head;
		
		while (curr.next != null) {
			if (curr.next.val == val) {
				curr.next = curr.next.next;
			}
			curr = curr.next;
			if (curr == null) break;
		}
		
		return head;
	}

	public static void main(String[] args) {
		ListNode b = new ListNode(8, new ListNode(9, new ListNode(4, new ListNode(9))));
		ListNode.display(b);
		ListNode.display(removeElements(b, 9));
		System.out.println("*************************");
		ListNode c = new ListNode(9, new ListNode(9));
		ListNode.display(c);
		ListNode.display(removeElements(c, 9));
	}

}
